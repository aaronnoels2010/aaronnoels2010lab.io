import { Injectable } from '@angular/core';
import { Topic } from '../models/topic';

@Injectable({
  providedIn: 'root'
})
export class TopicService {

  private topics: Topic[] = [{title: "Overzicht", url: "overview"},
  {title: "Aanpak", url: "approach"},
  {title: "Vervolg", url: "next"},
  {title: "Reflectie", url: "reflection"}];

  constructor() { }

  getAll(){
    return [...this.topics]
  }
}
