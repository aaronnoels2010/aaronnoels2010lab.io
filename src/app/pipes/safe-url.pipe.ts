import { Pipe, PipeTransform, SecurityContext } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';

@Pipe({
  name: 'safeURL'
})
export class SafeURLPipe implements PipeTransform {
  constructor(private sanitizer: DomSanitizer){}
  transform(url: unknown, ...args: unknown[]): unknown {
    return this.sanitizer.sanitize(SecurityContext.URL, url);
  }

}
