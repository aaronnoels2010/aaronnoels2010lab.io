import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { QuoteComponent } from '../components/templates/quote/quote.component';
import { NavigationComponent } from '../components/navigation/navigation.component';
import { PaginationComponent } from '../components/pagination/pagination.component';
import { ThreeOverviewComponent } from '../components/templates/three-overview/three-overview.component';
import { FourOverviewComponent } from '../components/templates/four-overview/four-overview.component';
import { DetailComponent } from '../components/templates/detail/detail.component';
import { TopicsDetailComponent } from '../components/templates/topics-detail/topics-detail.component';
import { Hexagon } from '../components/hexagon/hexagon.component';
import { HexagonCardComponent } from '../components/hexagon-card/hexagon-card.component';
import { ImageComponent } from '../components/templates/image/image.component';
import { VideoComponent } from '../components/templates/video/video.component';
import { SummaryPage } from '../pages/summary/summary.page';
import { OverviewComponent } from '../components/overview/overview.component';
import { HeaderComponent } from '../components/header/header.component';
import { CardComponent } from '../components/card/card.component';
import { RouterModule } from '@angular/router';
import { YouTubePlayerModule } from '@angular/youtube-player';



@NgModule({
  declarations: [SummaryPage, OverviewComponent, DetailComponent, ImageComponent, VideoComponent, TopicsDetailComponent, Hexagon, HexagonCardComponent, FourOverviewComponent, NavigationComponent, PaginationComponent, QuoteComponent, ThreeOverviewComponent, HeaderComponent, CardComponent],
  imports: [
    CommonModule,
    RouterModule,
    YouTubePlayerModule
  ],
  exports: [SummaryPage, OverviewComponent, DetailComponent, ImageComponent, VideoComponent, TopicsDetailComponent, Hexagon, HexagonCardComponent, FourOverviewComponent, NavigationComponent, PaginationComponent, QuoteComponent, ThreeOverviewComponent, HeaderComponent, CardComponent]
})

export class ShareModule { }
