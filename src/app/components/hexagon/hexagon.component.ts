import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-hexagon',
  templateUrl: './hexagon.component.html',
  styleUrls: ['./hexagon.component.scss'],
})
export class Hexagon implements OnInit {
  @Input() color:string ="blue";
  @Input() textData: string[] = [];
  @Input() image:string = "none.png";
  @Input() stroke:string = ""
  @Input() strokeWidth:string = "";
  @Input() textColor:string = "white";
  @Input() type: string = "default";
  @Input() x: string;
  @Input() y: string;
  @Input() xTablet: string;
  @Input() yTablet: string;
  @Input() xMedium: string;
  @Input() yMedium: string;
  @Input() xSmall: string;
  @Input() ySmall: string;
  imagePath: string;

  constructor() {
  }

  ngOnInit() {
    if(this.image == undefined || this.image == ""){
      this.imagePath = `/assets/images/none.png`
    } else {
      this.imagePath = `/assets/images/${this.image}`
    }
  }

}
