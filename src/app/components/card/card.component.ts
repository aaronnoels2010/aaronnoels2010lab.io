import { Component, OnInit, Input } from '@angular/core';
import { NavController } from '@ionic/angular';
import { Topic } from 'src/app/models/topic';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss'],
})
export class CardComponent implements OnInit {
  @Input() topic: Topic;
  @Input() icon: string; 
  
  constructor(private navController: NavController) { }

  ngOnInit() {}

  navigate(){
    this.navController.navigateForward(`/${this.topic.url}`)
  }
}
