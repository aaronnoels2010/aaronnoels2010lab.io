import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-topics-detail',
  templateUrl: './topics-detail.component.html',
  styleUrls: ['./topics-detail.component.scss'],
})
export class TopicsDetailComponent implements OnInit {
  @Input() backgroundColor: string = "grey";
  @Input() hexagonColor: string = "white";
  @Input() textColor: string = "grey"
  @Input() title: string = "Geen titel"
  @Input() topics: Object[] = [{image: "stripe.png"},
  {image: "stripe.png"},
  {image: "stripe.png"},
  {image: "stripe.png"},
  {image: "stripe.png"}];
  @Input() x: string = "50";
  @Input() y: string = "50";
  @Input() page: string = "0"
  @Input() pageKind: string;

  constructor() { }

  ngOnInit() {}

}
