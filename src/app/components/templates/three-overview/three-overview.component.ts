import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-three-overview',
  templateUrl: './three-overview.component.html',
  styleUrls: ['./three-overview.component.scss'],
})
export class ThreeOverviewComponent implements OnInit {
  @Input() backgroundColor: string = "grey";
  @Input() hexagonColor: string = "white";
  @Input() pageKind: string;
  @Input() page: string = "0";
  @Input() title: string = "Geen titel";
  @Input() topics: Object[] = [{title:"Anypoint Studio", description: "IDE", image: "salesforce.png"}, {title:"Anypoint Studio", description: "IDE"}, {title:"Anypoint Studio", description: "IDE"}]

  constructor() { }

  ngOnInit() {}

}
