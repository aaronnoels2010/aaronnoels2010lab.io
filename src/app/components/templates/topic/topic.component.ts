import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-topic',
  templateUrl: './topic.component.html',
  styleUrls: ['./topic.component.scss'],
})
export class TopicComponent implements OnInit {
  hexagonType: string = "default";
  @Input() title: string = "Geen titel";
  @Input() topic: Object = {title: ["test", "test", "test", "test", "test", "test"], color: "red", textColor: "white"};
  @Input() page: string = "0"
  @Input() pageKind: string;
  
  constructor() { }

  ngOnInit() {}

}
