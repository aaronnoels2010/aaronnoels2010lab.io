import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.scss'],
})
export class DetailComponent implements OnInit {
  @Input() background:string="grey";
  @Input() image:string = "salesforce.png";
  @Input() title:string = "Geen titel";
  @Input() page: string = "0"
  @Input() pageKind: string;
  source:string = "";

  constructor() {
  }

  ngOnInit() {
    this.source = `/assets/images/${this.image}`
  }

}
