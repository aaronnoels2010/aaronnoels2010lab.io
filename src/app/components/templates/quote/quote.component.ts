import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-quote',
  templateUrl: './quote.component.html',
  styleUrls: ['./quote.component.scss'],
})
export class QuoteComponent implements OnInit {
  @Input() quote: string = "Dit is de standaard quote"
  @Input() styling: string = "red"
  @Input() title: string = "Geen titel";
  @Input() page: string = "0"
  @Input() pageKind: string;

  constructor() { }

  ngOnInit() {}

}
