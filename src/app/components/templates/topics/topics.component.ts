import { Component, OnInit, HostListener, Input } from '@angular/core';

@Component({
  selector: 'app-topics',
  templateUrl: './topics.component.html',
  styleUrls: ['./topics.component.scss'],
})
export class TopicsComponent implements OnInit {
  @Input() page: string = "0"
  @Input() pageKind: string;
  hexagonType:string = "default";
  topics: Object[] = [{title: ["Wie"], color: "red"}, {title: ["Waar"], color: "white", strokeColor: "lightgrey"}, {title: ["Hoe?"], color: "Green"}];

  constructor() { }

  ngOnInit() {}


  @HostListener('window:resize', ['$event'])
  onResize(event) {
    if(window.innerWidth > 1000){
      this.hexagonType = "default";
    } else {
      this.hexagonType = "medium";
    }
  }
}
