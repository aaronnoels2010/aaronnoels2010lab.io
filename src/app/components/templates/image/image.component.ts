import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-image',
  templateUrl: './image.component.html',
  styleUrls: ['./image.component.scss'],
})
export class ImageComponent implements OnInit {
  @Input() title: string = "Geen titel";
  @Input() image: string = "salesforce.png";
  @Input() page: string = "0"
  @Input() pageKind: string;
  imagePath: string;

  constructor() { }

  ngOnInit() {
    this.imagePath = "/assets/images/" + this.image;
  }

}
