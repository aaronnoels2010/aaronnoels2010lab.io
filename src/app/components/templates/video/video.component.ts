import { Component, OnInit, Input, HostListener, SecurityContext } from '@angular/core';

@Component({
  selector: 'app-video',
  templateUrl: './video.component.html',
  styleUrls: ['./video.component.scss'],
})
export class VideoComponent implements OnInit {
  @Input() link: string = "ALDpTeZRFEM";
  @Input() title: string = "Geen titel";
  @Input() page: string = "0"
  @Input() pageKind: string;
  width: string = "800";
  height: string = "400";

  constructor() {
    this.checkSize();
  }

  ngOnInit() {
    this.checkSize();
  }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.checkSize();
  }

  checkSize(){
    console.log("Resize");
    if(window.innerWidth > 1000){
      this.height = "400"
      this.width = "800"
    } else if (window.innerWidth > 600){
      this.height = "250"
      this.width = "500"
    } else {
      this.height = "180"
      this.width = "360"
    }
  }

}
