import { Component, OnInit, Input, HostListener } from '@angular/core';

@Component({
  selector: 'app-four-overview',
  templateUrl: './four-overview.component.html',
  styleUrls: ['./four-overview.component.scss'],
})
export class FourOverviewComponent implements OnInit {
  @Input() backgroundColor: string;
  @Input() hexagonColor: string;
  @Input() title: string;
  @Input() topics: Object[] = [{title:"Anypoint Studio", description: "IDE", image: "salesforce.png"}, {title:"Anypoint Studio", description: "IDE"}, {title:"Anypoint Studio", description: "IDE"}, {title:"Anypoint Studio", description: "IDE"}]
  @Input() page: string = "0"
  @Input() pageKind: string;
  mobile: boolean = false;

  constructor() {
    this.checkSize();
  }

  ngOnInit() {
    this.checkSize();
    console.log(this.mobile)
  }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.checkSize();
  }

  checkSize(){
    console.log("Resize");
    if(window.innerWidth > 1000){
      this.mobile = false;
    } else {
      this.mobile = true;
    }
  }
}
