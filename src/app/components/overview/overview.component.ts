import { Component, OnInit, Input } from '@angular/core';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-overview',
  templateUrl: './overview.component.html',
  styleUrls: ['./overview.component.scss'],
})
export class OverviewComponent implements OnInit {
  @Input() topic: Object;
  imagePath: string;

  constructor(private navController: NavController) { }

  ngOnInit() {
    this.imagePath = `/assets/images/${this.topic["image"]}`
  }

  navigate(){
    this.navController.navigateForward(this.topic["link"])
  }

}
