import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-hexagon-card',
  templateUrl: './hexagon-card.component.html',
  styleUrls: ['./hexagon-card.component.scss'],
})
export class HexagonCardComponent implements OnInit {
  @Input() image: string;
  @Input() hexagonColor: string;
  @Input() textPosition: string = "start";
  @Input() title: string;
  @Input() description: string;
  @Input() type: string = "default";

  constructor() { }

  ngOnInit() {}

}
