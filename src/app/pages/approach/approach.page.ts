import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-approach',
  templateUrl: './approach.page.html',
  styleUrls: ['./approach.page.scss'],
})
export class ApproachPage implements OnInit {
  topics: Object[] = [{description: "Een kort overzicht van hoe het project concreet is aangepakt geweest en welke stappen er zijn ondernomen in elke fase ...", image: "approach.png", title: "Aanpak", link: "/approach/how"}, 
  {description: "Een samenvatting van de belangrijkste beslissingen die genomen zijn geweest gedurende het project ...", image: "decision.png", title: "Beslissingen", link: "/approach/why"}]

  constructor() { }

  ngOnInit() {
  }

}
