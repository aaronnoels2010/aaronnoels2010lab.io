import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ApproachPage } from './approach.page';

const routes: Routes = [
  {
    path: '',
    component: ApproachPage
  },
  {
    path: 'how',
    loadChildren: () => import('./how/how.module').then( m => m.HowPageModule)
  },
  {
    path: 'why',
    loadChildren: () => import('./why/why.module').then( m => m.WhyPageModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ApproachPageRoutingModule {}
