import { Component, OnInit, HostListener } from '@angular/core';

@Component({
  selector: 'app-how',
  templateUrl: './how.page.html',
  styleUrls: ['./how.page.scss'],
})
export class HowPage implements OnInit {
  isMobile: boolean = false;
  pageKind: string = "approach";
  flow: Object[] = [{title:"System API", description: `<span>Connectie rydoo</span>${this.isMobile ? "" : "<br/><br/><br/><br/><br/><br/><br/>"}`, image: "rydoo.png"}, 
  {title:"Proces API", description: '<img src="/assets/images/expense.png"/>', image: "database.png"}, 
  {title:"SalesForce", description: `<span>Aanmaken en updaten</span>${this.isMobile ? "" : "<br/><br/><br/><br/><br/><br/>"}`, image: "salesforce.png"}];

  constructor() {
  }

  ngOnInit() {
    this.checkType();
  }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    
  }

  checkType(){
    if(window.innerWidth > 1000){
      if(this.isMobile){
        this.isMobile = false;
        this.adjustFlow();
      }
    } else {
      if(!this.isMobile){
        this.isMobile = true;
        this.adjustFlow();
      }
    }
  }

  adjustFlow(){
    this.flow = [{title:"System API", description: `<span>Connectie rydoo</span>${this.isMobile ? "" : "<br/><br/><br/><br/><br/><br/><br/>"}`, image: "rydoo.png"}, 
    {title:"Proces API", description: '<img src="/assets/images/expense.png"/>', image: "database.png"}, 
    {title:"SalesForce", description: `<span>Aanmaken en updaten</span>${this.isMobile ? "" : "<br/><br/><br/><br/><br/><br/>"}`, image: "salesforce.png"}];
  }

}
