import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ApproachPage } from './approach.page';

describe('ApproachPage', () => {
  let component: ApproachPage;
  let fixture: ComponentFixture<ApproachPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ApproachPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ApproachPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
