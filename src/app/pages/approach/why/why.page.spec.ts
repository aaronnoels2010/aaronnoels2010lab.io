import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { WhyPage } from './why.page';

describe('WhyPage', () => {
  let component: WhyPage;
  let fixture: ComponentFixture<WhyPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WhyPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(WhyPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
