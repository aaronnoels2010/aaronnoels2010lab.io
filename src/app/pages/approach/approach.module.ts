import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ApproachPageRoutingModule } from './approach-routing.module';
import { ApproachPage } from './approach.page';
import { ShareModule } from 'src/app/share/share.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ApproachPageRoutingModule,
    ShareModule
  ],
  declarations: [ApproachPage]
})
export class ApproachPageModule {}
