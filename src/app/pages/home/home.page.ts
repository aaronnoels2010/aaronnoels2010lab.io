import { Component } from '@angular/core';
import { TopicService } from 'src/app/services/topic.service';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  topics: Object[]
  scan: string = '<span style="{text-align: center}">Scan or click me!</span>';

  constructor(private topicService: TopicService) {
    this.topics = topicService.getAll();
  }

}
