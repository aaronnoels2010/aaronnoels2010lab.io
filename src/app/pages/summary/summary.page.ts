import { Component, OnInit, Input } from '@angular/core';
import { Topic } from 'src/app/models/topic';

@Component({
  selector: 'app-summary',
  templateUrl: './summary.page.html',
  styleUrls: ['./summary.page.scss'],
})
export class SummaryPage implements OnInit {
  @Input() topics: Object[];

  constructor() { }

  ngOnInit() {
  }

}
