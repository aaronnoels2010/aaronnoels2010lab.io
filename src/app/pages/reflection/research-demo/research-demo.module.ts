import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ResearchDemoPageRoutingModule } from './research-demo-routing.module';

import { ResearchDemoPage } from './research-demo.page';
import { ShareModule } from 'src/app/share/share.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ResearchDemoPageRoutingModule,
    ShareModule
  ],
  declarations: [ResearchDemoPage]
})
export class ResearchDemoPageModule {}
