import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-research-demo',
  templateUrl: './research-demo.page.html',
  styleUrls: ['./research-demo.page.scss'],
})
export class ResearchDemoPage implements OnInit {
  pageKind: string = "reflection";
  video: string = "WEm_l_I94bk";

  constructor() { }

  ngOnInit() {
  }

}
