import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ResearchDemoPage } from './research-demo.page';

describe('ResearchDemoPage', () => {
  let component: ResearchDemoPage;
  let fixture: ComponentFixture<ResearchDemoPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ResearchDemoPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ResearchDemoPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
