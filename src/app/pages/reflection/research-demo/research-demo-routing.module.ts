import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ResearchDemoPage } from './research-demo.page';

const routes: Routes = [
  {
    path: '',
    component: ResearchDemoPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ResearchDemoPageRoutingModule {}
