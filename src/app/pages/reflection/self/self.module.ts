import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SelfPageRoutingModule } from './self-routing.module';

import { SelfPage } from './self.page';
import { ShareModule } from 'src/app/share/share.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SelfPageRoutingModule,
    ShareModule
  ],
  declarations: [SelfPage]
})
export class SelfPageModule {}
