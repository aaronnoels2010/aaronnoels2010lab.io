import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-reflection',
  templateUrl: './reflection.page.html',
  styleUrls: ['./reflection.page.scss'],
})
export class ReflectionPage implements OnInit {
  topics: Object[] = [{description: "Een korte trailer van de SalesForce omgeving en wat hier concreet in ontwikkeld is geweest ...", image: "salesforce.png", title: "Demo stage", link: "/reflection/internship-demo"}, 
  {description: "Een korte trailer van de demo API die ontwikkeld is geweest op basis van de onderzochte topics gedurende het onderzoek ...", image: "research-video.svg", title: "Demo research", link: "/reflection/research-demo"}, 
  {description: "Een reflectie van de opgedane kennis gedurende de stage en welke conclusies ik hierzelf heb uit getrokken ...", image: "reflectie.png", title: "Conclusie", link: "/reflection/self"}]

  constructor() { }

  ngOnInit() {
  }

}
