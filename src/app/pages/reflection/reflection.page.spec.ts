import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ReflectionPage } from './reflection.page';

describe('ReflectionPage', () => {
  let component: ReflectionPage;
  let fixture: ComponentFixture<ReflectionPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReflectionPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ReflectionPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
