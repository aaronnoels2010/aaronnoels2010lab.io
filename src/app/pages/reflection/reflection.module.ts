import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ReflectionPageRoutingModule } from './reflection-routing.module';

import { ReflectionPage } from './reflection.page';
import { ShareModule } from 'src/app/share/share.module';
import { YouTubePlayerModule } from '@angular/youtube-player';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReflectionPageRoutingModule,
    ShareModule
  ],
  declarations: [ReflectionPage]
})
export class ReflectionPageModule {}
