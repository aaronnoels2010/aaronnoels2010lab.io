import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ReflectionPage } from './reflection.page';

const routes: Routes = [
  {
    path: '',
    component: ReflectionPage
  },
  {
    path: 'internship-demo',
    loadChildren: () => import('./internship-demo/internship-demo.module').then( m => m.InternshipDemoPageModule)
  },
  {
    path: 'research-demo',
    loadChildren: () => import('./research-demo/research-demo.module').then( m => m.ResearchDemoPageModule)
  },
  {
    path: 'self',
    loadChildren: () => import('./self/self.module').then( m => m.SelfPageModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ReflectionPageRoutingModule {}
