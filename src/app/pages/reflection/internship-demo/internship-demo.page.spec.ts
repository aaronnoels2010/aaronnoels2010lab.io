import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { InternshipDemoPage } from './internship-demo.page';

describe('InternshipDemoPage', () => {
  let component: InternshipDemoPage;
  let fixture: ComponentFixture<InternshipDemoPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InternshipDemoPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(InternshipDemoPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
