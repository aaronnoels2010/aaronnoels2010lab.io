import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-internship-demo',
  templateUrl: './internship-demo.page.html',
  styleUrls: ['./internship-demo.page.scss'],
})
export class InternshipDemoPage implements OnInit {
  pageKind: string = "reflection";
  video: string = "vS3XQ9OCHKM";

  constructor() { }

  ngOnInit() {
  }

}
