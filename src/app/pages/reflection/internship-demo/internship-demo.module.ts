import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { InternshipDemoPageRoutingModule } from './internship-demo-routing.module';

import { InternshipDemoPage } from './internship-demo.page';
import { SafeURLPipe } from 'src/app/pipes/safe-url.pipe';
import { ShareModule } from 'src/app/share/share.module';
import { YouTubePlayerModule } from '@angular/youtube-player';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    InternshipDemoPageRoutingModule,
    ShareModule
  ],
  declarations: [InternshipDemoPage, SafeURLPipe]
})
export class InternshipDemoPageModule {}
