import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { InternshipDemoPage } from './internship-demo.page';

const routes: Routes = [
  {
    path: '',
    component: InternshipDemoPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class InternshipDemoPageRoutingModule {}
