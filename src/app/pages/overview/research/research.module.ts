import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ResearchPageRoutingModule } from './research-routing.module';

import { ResearchPage } from './research.page';
import { ShareModule } from 'src/app/share/share.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ResearchPageRoutingModule,
    ShareModule
  ],
  declarations: [ResearchPage]
})
export class ResearchPageModule {}
