import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-research',
  templateUrl: './research.page.html',
  styleUrls: ['./research.page.scss'],
})
export class ResearchPage implements OnInit {
  pageKind: string = "overview";
  apis: Object[] = [{title:"NASA", description: "<ul><li>Astronomische data</li><li>Communicatie via browser</li></ul>", image:"nasa.png"}, 
  {title:"Twilio", description: "<ul><li>Communicatie</li><li>Webhooks</li></ul>", image:"twilio.png"}, 
  {title:"Stripe", description: "<ul><li>Betaal API</li><li>Twee implementaties</li></ul>", image:"stripe.png"}, 
  {title:"Twitter", description: "<ul><li>Sociale media</li><li>Use cases</li></ul>", image:"twitter.png"}];
  protocols: Object[] = [{title:"SOAP", description: "<ul><li>Lang standaard</li><li>Procedure</li><li>XML</li></ul>", image: "SOAP.png"}, 
  {title:"REST", description: "<ul><li>Richtlijn</li><li>Voornamelijk JSON</li><li>Varianten</li></ul>", image: "REST.png"}, 
  {title:"GraphQL", description: "<ul><li>Facebook</li><li>Query</li><li>JSON</li></ul>", image: "graphql.png"}];
  dataModels: Object[] = [{title:"CSV", description: "<ul><li>ML</li><li>Compact</li><li>Moeilijk diepgang</li></ul>", image:"csv.png"}, 
  {title:"JSON", description: "<ul><li>Standaard</li><li>Leesbaar</li><li>Geen metadata</li></ul>", image:"JSON.png"}, 
  {title:"XML", description: "<ul><li>Lang de standaard</li><li>Leesbaar</li><li>Verbose</li></ul>", image:"xml.png"}, 
  {title:"ProtoBuf", description: "<ul><li>Nieuw</li><li>Bytes</li><li>Onleesbaar</li></ul>", image:"protobuf.png"}]

  constructor() { }

  ngOnInit() {
  }

}
