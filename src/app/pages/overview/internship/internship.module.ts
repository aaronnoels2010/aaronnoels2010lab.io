import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { InternshipPageRoutingModule } from './internship-routing.module';

import { InternshipPage } from './internship.page';
import { ShareModule } from 'src/app/share/share.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    InternshipPageRoutingModule,
    ShareModule
  ],
  declarations: [InternshipPage]
})
export class InternshipPageModule {}
