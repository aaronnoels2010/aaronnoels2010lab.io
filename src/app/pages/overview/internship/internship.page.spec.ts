import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { InternshipPage } from './internship.page';

describe('InternshipPage', () => {
  let component: InternshipPage;
  let fixture: ComponentFixture<InternshipPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InternshipPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(InternshipPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
