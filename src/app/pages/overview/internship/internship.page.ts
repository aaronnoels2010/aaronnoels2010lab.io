import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-internship',
  templateUrl: './internship.page.html',
  styleUrls: ['./internship.page.scss'],
})
export class InternshipPage implements OnInit {
  pageKind: string = "overview";
  inhoudsTafel: Object = {title: ["1. Bedrijfsvoorstelling", "2. Probleem", "3. Doel", "4. Technologie", "5. Flow", "6. SalesForce"], textColor: "white", color: "#db4238"}
  technologies: Object[] = [{title:"Anypoint Studio", description: "IDE", image:"anypoint-studio.png"}, {title:"SalesForce", description: "CRM tool", image:"salesforce.png"}, {title:"Anypoint Platform", description: "IPaas", image:"mulesoft.png"}, {title:"GitLab", description: "Project cyclus", image:"gitlab.png"}]
  flow: Object[] = [{title:"System API", description: "Connectie service", image: "service-blue.png"}, {title:"Proces API", description: "Canoniek datamodel", image: "database.png"}, {title:"Experience API", description: "Externe Toepassingen", image: "avatar.png"}];
  flowOverview: Object[] = [{image: "SDWorx.png"},
  {image: "service.png"},
  {image: "wordpress.png"},
  {image: "rydoo.png"},
  {image: "survey-monkey.png"}];

  constructor() { }

  ngOnInit() {
  }

}
