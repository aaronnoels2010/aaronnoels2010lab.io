import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-overview-page',
  templateUrl: './overview.page.html',
  styleUrls: ['./overview.page.scss'],
})
export class OverviewPage implements OnInit {
  topics: Object[] = [{description:"De stage is een integratieverhaal over het integreren van externe services door middel van MuleSoft en deze data vervolgens te consolideren in SalesForce ...", image: "salesforce.png", title: "Stage", link: "/overview/internship"}, 
  {description: "Het onderzoek tijdens deze stage focust zich op de ontwikkeling van een API en onderzoekt verschillende factoren aan de hand van populaire API's ...", image: "api.png", title: "Onderzoek", link: "/overview/research"}]

  constructor() { }

  ngOnInit() {
  }

}
